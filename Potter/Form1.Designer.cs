﻿namespace Potter
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.questStart = new System.Windows.Forms.Button();
            this.orderListLabel = new System.Windows.Forms.Label();
            this.playerBackground = new AxWMPLib.AxWindowsMediaPlayer();
            this.playerEvents = new AxWMPLib.AxWindowsMediaPlayer();
            this.playTimer = new System.Windows.Forms.Timer(this.components);
            this.orderList = new System.Windows.Forms.RichTextBox();
            this.portNumber = new System.Windows.Forms.TextBox();
            this.portNumberLabel = new System.Windows.Forms.Label();
            this.start = new System.Windows.Forms.Button();
            this.playHappyBirthday = new System.Windows.Forms.Button();
            this.mic = new System.Windows.Forms.Button();
            this.eventTimed = new System.Windows.Forms.Timer(this.components);
            this.boiOkonchenDobbi = new System.Windows.Forms.Button();
            this.boiPrervanPlayer = new AxWMPLib.AxWindowsMediaPlayer();
            this.consoleList = new System.Windows.Forms.RichTextBox();
            this.checkDebug = new System.Windows.Forms.CheckBox();
            this.labelQuestStartTime = new System.Windows.Forms.Label();
            this.labelQuestGameTime = new System.Windows.Forms.Label();
            this.buttonStopTime = new System.Windows.Forms.Button();
            this.timerTime = new System.Windows.Forms.Timer(this.components);
            this.orderListClear = new System.Windows.Forms.Button();
            this.buttonPodarokSerial = new System.Windows.Forms.Button();
            this.buttonDobbyReset = new System.Windows.Forms.Button();
            this.buttonOpenGreenCasket = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.playerBackground)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerEvents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.boiPrervanPlayer)).BeginInit();
            this.SuspendLayout();
            // 
            // questStart
            // 
            this.questStart.Enabled = false;
            this.questStart.Location = new System.Drawing.Point(157, 23);
            this.questStart.Name = "questStart";
            this.questStart.Size = new System.Drawing.Size(90, 26);
            this.questStart.TabIndex = 0;
            this.questStart.Text = "КВЕСТ СТАРТ";
            this.questStart.UseVisualStyleBackColor = true;
            this.questStart.Click += new System.EventHandler(this.button1_Click);
            // 
            // orderListLabel
            // 
            this.orderListLabel.AutoSize = true;
            this.orderListLabel.Location = new System.Drawing.Point(414, 9);
            this.orderListLabel.Name = "orderListLabel";
            this.orderListLabel.Size = new System.Drawing.Size(116, 13);
            this.orderListLabel.TabIndex = 1;
            this.orderListLabel.Text = "Ожидающие события";
            // 
            // playerBackground
            // 
            this.playerBackground.Enabled = true;
            this.playerBackground.Location = new System.Drawing.Point(12, 260);
            this.playerBackground.Name = "playerBackground";
            this.playerBackground.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("playerBackground.OcxState")));
            this.playerBackground.Size = new System.Drawing.Size(585, 94);
            this.playerBackground.TabIndex = 2;
            // 
            // playerEvents
            // 
            this.playerEvents.Enabled = true;
            this.playerEvents.Location = new System.Drawing.Point(12, 162);
            this.playerEvents.Name = "playerEvents";
            this.playerEvents.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("playerEvents.OcxState")));
            this.playerEvents.Size = new System.Drawing.Size(585, 92);
            this.playerEvents.TabIndex = 3;
            this.playerEvents.Visible = false;
            // 
            // playTimer
            // 
            this.playTimer.Tick += new System.EventHandler(this.playTimer_Tick);
            // 
            // orderList
            // 
            this.orderList.Location = new System.Drawing.Point(417, 23);
            this.orderList.Name = "orderList";
            this.orderList.ReadOnly = true;
            this.orderList.Size = new System.Drawing.Size(180, 82);
            this.orderList.TabIndex = 4;
            this.orderList.Text = "";
            // 
            // portNumber
            // 
            this.portNumber.Location = new System.Drawing.Point(9, 56);
            this.portNumber.Name = "portNumber";
            this.portNumber.Size = new System.Drawing.Size(100, 20);
            this.portNumber.TabIndex = 5;
            // 
            // portNumberLabel
            // 
            this.portNumberLabel.Location = new System.Drawing.Point(9, 23);
            this.portNumberLabel.Name = "portNumberLabel";
            this.portNumberLabel.Size = new System.Drawing.Size(118, 26);
            this.portNumberLabel.TabIndex = 6;
            this.portNumberLabel.Text = "Введите № порта и нажмите \"Старт\"";
            // 
            // start
            // 
            this.start.Location = new System.Drawing.Point(9, 82);
            this.start.Name = "start";
            this.start.Size = new System.Drawing.Size(100, 23);
            this.start.TabIndex = 7;
            this.start.Text = "СТАРТ";
            this.start.UseVisualStyleBackColor = true;
            this.start.Click += new System.EventHandler(this.start_Click);
            // 
            // playHappyBirthday
            // 
            this.playHappyBirthday.Enabled = false;
            this.playHappyBirthday.Location = new System.Drawing.Point(290, 67);
            this.playHappyBirthday.Name = "playHappyBirthday";
            this.playHappyBirthday.Size = new System.Drawing.Size(90, 38);
            this.playHappyBirthday.TabIndex = 8;
            this.playHappyBirthday.Text = "ПОДАРОК";
            this.playHappyBirthday.UseVisualStyleBackColor = true;
            this.playHappyBirthday.Click += new System.EventHandler(this.playHappyBirthday_Click);
            // 
            // mic
            // 
            this.mic.Enabled = false;
            this.mic.Image = global::Potter.Properties.Resources.ic_mic_off;
            this.mic.Location = new System.Drawing.Point(157, 52);
            this.mic.Name = "mic";
            this.mic.Size = new System.Drawing.Size(90, 53);
            this.mic.TabIndex = 9;
            this.mic.UseVisualStyleBackColor = true;
            this.mic.Click += new System.EventHandler(this.mic_Click);
            // 
            // eventTimed
            // 
            this.eventTimed.Tick += new System.EventHandler(this.eventTimed_Tick);
            // 
            // boiOkonchenDobbi
            // 
            this.boiOkonchenDobbi.Enabled = false;
            this.boiOkonchenDobbi.Location = new System.Drawing.Point(290, 23);
            this.boiOkonchenDobbi.Name = "boiOkonchenDobbi";
            this.boiOkonchenDobbi.Size = new System.Drawing.Size(90, 38);
            this.boiOkonchenDobbi.TabIndex = 10;
            this.boiOkonchenDobbi.Text = "ДОББИ ПОДОЗВАТЬ";
            this.boiOkonchenDobbi.UseVisualStyleBackColor = true;
            this.boiOkonchenDobbi.Click += new System.EventHandler(this.boiOkonchenDobbi_Click);
            // 
            // boiPrervanPlayer
            // 
            this.boiPrervanPlayer.Enabled = true;
            this.boiPrervanPlayer.Location = new System.Drawing.Point(536, 9);
            this.boiPrervanPlayer.Name = "boiPrervanPlayer";
            this.boiPrervanPlayer.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("boiPrervanPlayer.OcxState")));
            this.boiPrervanPlayer.Size = new System.Drawing.Size(585, 92);
            this.boiPrervanPlayer.TabIndex = 11;
            this.boiPrervanPlayer.Visible = false;
            // 
            // consoleList
            // 
            this.consoleList.Location = new System.Drawing.Point(12, 362);
            this.consoleList.Name = "consoleList";
            this.consoleList.ReadOnly = true;
            this.consoleList.Size = new System.Drawing.Size(487, 63);
            this.consoleList.TabIndex = 12;
            this.consoleList.Text = "";
            this.consoleList.Visible = false;
            // 
            // checkDebug
            // 
            this.checkDebug.AutoSize = true;
            this.checkDebug.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.checkDebug.Location = new System.Drawing.Point(12, 131);
            this.checkDebug.Name = "checkDebug";
            this.checkDebug.Size = new System.Drawing.Size(163, 17);
            this.checkDebug.TabIndex = 13;
            this.checkDebug.Text = "Показать отладочное окно";
            this.checkDebug.UseVisualStyleBackColor = true;
            this.checkDebug.CheckedChanged += new System.EventHandler(this.checkDebug_CheckedChanged);
            // 
            // labelQuestStartTime
            // 
            this.labelQuestStartTime.AutoSize = true;
            this.labelQuestStartTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelQuestStartTime.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.labelQuestStartTime.Location = new System.Drawing.Point(414, 117);
            this.labelQuestStartTime.Name = "labelQuestStartTime";
            this.labelQuestStartTime.Size = new System.Drawing.Size(63, 13);
            this.labelQuestStartTime.TabIndex = 14;
            this.labelQuestStartTime.Text = "НАЧАЛО:";
            this.labelQuestStartTime.Visible = false;
            // 
            // labelQuestGameTime
            // 
            this.labelQuestGameTime.AutoSize = true;
            this.labelQuestGameTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelQuestGameTime.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.labelQuestGameTime.Location = new System.Drawing.Point(414, 141);
            this.labelQuestGameTime.Name = "labelQuestGameTime";
            this.labelQuestGameTime.Size = new System.Drawing.Size(65, 13);
            this.labelQuestGameTime.TabIndex = 15;
            this.labelQuestGameTime.Text = "ПРОШЛО:";
            this.labelQuestGameTime.Visible = false;
            // 
            // buttonStopTime
            // 
            this.buttonStopTime.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.buttonStopTime.Location = new System.Drawing.Point(572, 125);
            this.buttonStopTime.Name = "buttonStopTime";
            this.buttonStopTime.Size = new System.Drawing.Size(25, 23);
            this.buttonStopTime.TabIndex = 16;
            this.buttonStopTime.Text = "Х";
            this.buttonStopTime.UseVisualStyleBackColor = true;
            this.buttonStopTime.Visible = false;
            this.buttonStopTime.Click += new System.EventHandler(this.buttonStopTime_Click);
            // 
            // timerTime
            // 
            this.timerTime.Tick += new System.EventHandler(this.timerTime_Tick);
            // 
            // orderListClear
            // 
            this.orderListClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.orderListClear.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.orderListClear.Location = new System.Drawing.Point(505, 360);
            this.orderListClear.Name = "orderListClear";
            this.orderListClear.Size = new System.Drawing.Size(92, 65);
            this.orderListClear.TabIndex = 17;
            this.orderListClear.Text = "Удалить последнее событие";
            this.orderListClear.UseVisualStyleBackColor = false;
            this.orderListClear.Visible = false;
            this.orderListClear.Click += new System.EventHandler(this.orderListClear_Click);
            // 
            // buttonPodarokSerial
            // 
            this.buttonPodarokSerial.Location = new System.Drawing.Point(12, 431);
            this.buttonPodarokSerial.Name = "buttonPodarokSerial";
            this.buttonPodarokSerial.Size = new System.Drawing.Size(81, 27);
            this.buttonPodarokSerial.TabIndex = 18;
            this.buttonPodarokSerial.Text = "Подарок";
            this.buttonPodarokSerial.UseVisualStyleBackColor = true;
            this.buttonPodarokSerial.Visible = false;
            this.buttonPodarokSerial.Click += new System.EventHandler(this.buttonPodarokSerial_Click);
            // 
            // buttonDobbyReset
            // 
            this.buttonDobbyReset.Location = new System.Drawing.Point(99, 431);
            this.buttonDobbyReset.Name = "buttonDobbyReset";
            this.buttonDobbyReset.Size = new System.Drawing.Size(87, 27);
            this.buttonDobbyReset.TabIndex = 19;
            this.buttonDobbyReset.Text = "Добби reset";
            this.buttonDobbyReset.UseVisualStyleBackColor = true;
            this.buttonDobbyReset.Visible = false;
            this.buttonDobbyReset.Click += new System.EventHandler(this.buttonDobbyReset_Click);
            // 
            // buttonOpenGreenCasket
            // 
            this.buttonOpenGreenCasket.Location = new System.Drawing.Point(191, 431);
            this.buttonOpenGreenCasket.Name = "buttonOpenGreenCasket";
            this.buttonOpenGreenCasket.Size = new System.Drawing.Size(84, 27);
            this.buttonOpenGreenCasket.TabIndex = 20;
            this.buttonOpenGreenCasket.Text = "Откр зел шк";
            this.buttonOpenGreenCasket.UseVisualStyleBackColor = true;
            this.buttonOpenGreenCasket.Visible = false;
            this.buttonOpenGreenCasket.Click += new System.EventHandler(this.buttonOpenGreenCasket_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(609, 361);
            this.Controls.Add(this.buttonOpenGreenCasket);
            this.Controls.Add(this.buttonDobbyReset);
            this.Controls.Add(this.buttonPodarokSerial);
            this.Controls.Add(this.orderListClear);
            this.Controls.Add(this.buttonStopTime);
            this.Controls.Add(this.labelQuestGameTime);
            this.Controls.Add(this.labelQuestStartTime);
            this.Controls.Add(this.checkDebug);
            this.Controls.Add(this.consoleList);
            this.Controls.Add(this.boiOkonchenDobbi);
            this.Controls.Add(this.mic);
            this.Controls.Add(this.playHappyBirthday);
            this.Controls.Add(this.start);
            this.Controls.Add(this.portNumberLabel);
            this.Controls.Add(this.portNumber);
            this.Controls.Add(this.orderList);
            this.Controls.Add(this.playerEvents);
            this.Controls.Add(this.playerBackground);
            this.Controls.Add(this.orderListLabel);
            this.Controls.Add(this.questStart);
            this.Controls.Add(this.boiPrervanPlayer);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.Name = "Form1";
            this.Text = "Potter";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.playerBackground)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerEvents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.boiPrervanPlayer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button questStart;
        private System.Windows.Forms.Label orderListLabel;
        private AxWMPLib.AxWindowsMediaPlayer playerBackground;
        private AxWMPLib.AxWindowsMediaPlayer playerEvents;
        private System.Windows.Forms.Timer playTimer;
        private System.Windows.Forms.RichTextBox orderList;
        private System.Windows.Forms.TextBox portNumber;
        private System.Windows.Forms.Label portNumberLabel;
        private System.Windows.Forms.Button start;
        private System.Windows.Forms.Button playHappyBirthday;
        private System.Windows.Forms.Button mic;
        private System.Windows.Forms.Timer eventTimed;
        private System.Windows.Forms.Button boiOkonchenDobbi;
        private AxWMPLib.AxWindowsMediaPlayer boiPrervanPlayer;
        private System.Windows.Forms.RichTextBox consoleList;
        private System.Windows.Forms.CheckBox checkDebug;
        private System.Windows.Forms.Label labelQuestStartTime;
        private System.Windows.Forms.Label labelQuestGameTime;
        private System.Windows.Forms.Button buttonStopTime;
        private System.Windows.Forms.Timer timerTime;
        private System.Windows.Forms.Button orderListClear;
        private System.Windows.Forms.Button buttonPodarokSerial;
        private System.Windows.Forms.Button buttonDobbyReset;
        private System.Windows.Forms.Button buttonOpenGreenCasket;
    }
}

