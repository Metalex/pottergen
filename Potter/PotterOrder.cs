﻿using System.Collections.Generic;

namespace Potter
{
    internal class PotterOrder
    {
        private Queue<PotterEvent> order = new Queue<PotterEvent>();

        public void add(PotterEvent file)
        {
            order.Enqueue(file);
        }

        public PotterEvent pop()
        {   // УДАЛЯЕТ ИЗ НАЧАЛА
            return isEmpty() ? null : order.Dequeue();
        }

        public PotterEvent peek()
        {   // НЕ УДАЛЯЕТ ИЗ НАЧАЛА
            return isEmpty() ? null : order.Peek();
        }

        public void removeLast()
        {
            if (order.Count <= 1)
            {
                return;
            }
            int lastItem = order.Count;
            Queue<PotterEvent> newOrder = new Queue<PotterEvent>();
            foreach (PotterEvent orderItem in order)
            {
                if (--lastItem == 0)
                {
                    // мы подошли к последнему элементу
                    continue;
                }
                newOrder.Enqueue(orderItem);
            }
            order.Clear();
            foreach (PotterEvent orderItem in newOrder)
            {
                order.Enqueue(orderItem);
            }
        }

        public void clear()
        {
            order.Clear();
        }

        public bool isEmpty()
        {
            return order.Count == 0;
        }

        public Queue<PotterEvent>.Enumerator GetEnumerator()
        {
            return order.GetEnumerator();
        }
    }
}