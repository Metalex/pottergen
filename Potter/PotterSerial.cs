﻿using System;
using System.IO.Ports;
using System.Windows.Forms;

namespace Potter
{
    // Делегат используется для записи в UI control из потока не-UI
    public delegate void FromSerialToUi(string text);

    internal class PotterSerial
    {

        private SerialPort serial;
        private Action<string> serialEventCallback;


        public PotterSerial(int portNumber)
        {
            // Все опции для последовательного устройства
            // ---- могут быть отправлены через конструктор класса SerialPort
            // ---- PortName = "COM1", Baud Rate = 19200, Parity = None,
            // ---- Data Bits = 8, Stop Bits = One, Handshake = None
            serial = new SerialPort("COM"+portNumber,
                                                    28800,
                                                    Parity.None,
                                                    8,
                                                    StopBits.One);
            serial.Handshake = Handshake.None;
        }

        public void addEventHandler(Action<string> callback)
        {
            this.serialEventCallback = callback;
            serial.DataReceived += new SerialDataReceivedEventHandler(onSerialEvent);
        }

        private void onSerialEvent(object sender, SerialDataReceivedEventArgs e)
        {
            string data = serial.ReadExisting();
            serialEventCallback(data);
        }

        public void open() 
        {
            serial.Open();
        }

        public void close()
        {
            serial.Close();
        }

        public bool isOpen()
        {
            return serial.IsOpen;
        }

        public void write(string text)
        {
            serial.Write(text);
        }

        public void write(int number)
        {
            byte[] b = BitConverter.GetBytes(number);
            serial.Write(b, 0, b.Length);
        }
    }
}