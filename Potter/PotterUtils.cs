﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Potter
{
    internal class PotterUtils
    {

        private static readonly Lazy<PotterUtils> lazy = new Lazy<PotterUtils>(() => new PotterUtils());
        public static PotterUtils Instance { get { return lazy.Value; } }
        private PotterUtils() { }

        public long getMillis()
        {
            return DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
        }

        public String getTime()
        {
            var date = DateTime.Now;
            String millis = date.Millisecond + "";
            var length = millis.Length;
            for (int i = 0; i < 3 - length; i++)
            {
                millis += "0";
            }
            return (date.Hour < 10 ? "0" + date.Hour : "" + date.Hour) + ":" +
                (date.Minute < 10 ? "0" + date.Minute : "" + date.Minute) + ":" +
                (date.Second < 10 ? "0" + date.Second : "" + date.Second) + "." +
                millis;
        }

        public String generalMillisToTime(long millis)
        {
            DateTime date = new DateTime(1970, 1, 1) + TimeSpan.FromMilliseconds(millis);
            return (date.Hour < 10 ? "0" + date.Hour : "" + date.Hour) + ":" +
                (date.Minute < 10 ? "0" + date.Minute : "" + date.Minute) + ":" +
                (date.Second < 10 ? "0" + date.Second : "" + date.Second);
        }

        public String elapsedMillisToTime(long millis)
        {
            TimeSpan timeSpan = TimeSpan.FromMilliseconds(millis);
            int hours = (int)timeSpan.TotalHours;
            int mins = (int)timeSpan.TotalMinutes - hours * 60;
            int secs = (int)timeSpan.TotalSeconds - hours * 60 - mins * 60;
            return (hours < 10 ? "0" + hours : "" + hours) + ":" +
                (mins < 10 ? "0" + mins : "" + mins) + ":" +
                (secs < 10 ? "0" + secs : "" + secs);
        }
    }
}
