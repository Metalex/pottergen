﻿using System;

namespace Potter
{

    internal class PotterEvent
    {
        // События, которые могут приходить с сериал порта
        public enum SerialIn
        {
            BANOCHKA_ZELIYA_P,
            OTKRITIE_SUNDUKA_1,
            OTKRITIE_ZASLONKI_LABIRINTA_2,
            USER_BERET_KNIGU_3,
            PODNESENIE_NOSKA_4,
            ANEKDOT_DOBBI_1_5,
            ANEKDOT_DOBBI_2_6,
            ANEKDOT_DOBBI_3_7,
            ANEKDOT_DOBBI_4_8,
            OTKRITIE_SKATULKI_DOBBI_9,
            NAGATIE_KIRPICHA_A,
            KIRPICHI_RAZGADANY_B,
            TRI_SHARA_SOBRANY_C,
            PROHOGDENIE_SHAHMAT_D,
            BOI_NACHAT_VOZOBNOVLEN_E,
            BOI_PRERVAN_G,
            BOI_SILA_SLIZERINA_L,
            BOI_SILA_KOGTEVRAN_M,
            BOI_SILA_GRIFFINDORA_N            
        }

        // События, которые могут уходить на сериал порт. 
        // VOSPOMINANIE_* принадлежит типу EVENT
        // PODAROK_4 принадлежит типу TIMED
        public enum SerialOut
        {
            VOSPOMINANIE_OFF_0,
            VOSPOMINANIE_G_1,
            VOSPOMINANIE_B_2,
            VOSPOMINANIE_R_3,
            PODAROK_4,
            KAZAN_START_5,
            DOBBY_RESET_6,
            OPEN_GREEN_CASKET_7,
            EMPTY
        }

        // События, которые могут вызываться вручную с этой программы
        public enum Manual
        {
            QUEST_START,
            HAPPY_BIRTHDAY,
            BOI_OKONCHEN_DOBBI
        }

        // События, которые могут вызываться по истечению определенного времени
        public enum Timed
        {
            BOI_START,
            BOI_PODSKAZKA,
            BOI_POBEDA
        }

        public enum Type
        {
            TIMED,
            MANUAL,
            EVENT
        }

        public readonly Type type;

        public readonly SerialIn serialIn; // Может быть только у типа EVENT 
        private SerialOut serialOut; // Может быть только у типа EVENT

        public readonly Timed eventTimed; // Может быть только у типа TIMED
        public readonly int delaySeconds; // Может быть только у типа TIMED

        public readonly Manual eventManual; // Может быть только у типа MANUAL
        private SerialOut serialOutManual; // Может быть только у типа MANUAL

        public PotterEvent(String text)
        {
            type = Type.EVENT;
            try {
                int index = int.Parse(text);
                serialIn = (SerialIn)index;
            } catch (FormatException e)
            {
                // события с буквой
                if (text.Equals("a"))
                {
                    serialIn = SerialIn.NAGATIE_KIRPICHA_A;
                } else if (text.Equals("b"))
                {
                    serialIn = SerialIn.KIRPICHI_RAZGADANY_B;
                } else if (text.Equals("c"))
                {
                    serialIn = SerialIn.TRI_SHARA_SOBRANY_C;
                } else if (text.Equals("d"))
                {
                    serialIn = SerialIn.PROHOGDENIE_SHAHMAT_D;
                } else if (text.Equals("e"))
                {
                    serialIn = SerialIn.BOI_NACHAT_VOZOBNOVLEN_E;
                } else if (text.Equals("g"))
                {
                    serialIn = SerialIn.BOI_PRERVAN_G;
                } else if (text.Equals("l"))
                {
                    serialIn = SerialIn.BOI_SILA_SLIZERINA_L;
                } else if (text.Equals("m"))
                {
                    serialIn = SerialIn.BOI_SILA_KOGTEVRAN_M;
                } else if (text.Equals("n"))
                {
                    serialIn = SerialIn.BOI_SILA_GRIFFINDORA_N;
                } else if (text.Equals("p"))
                {
                    serialIn = SerialIn.BANOCHKA_ZELIYA_P;
                }
            }
            serialOut = getSerialOut();
        }

        public PotterEvent(Manual manual, SerialOut serialOutManual)
        {
            type = Type.MANUAL;
            eventManual = manual;
            this.serialOutManual = serialOutManual;
        }

        public PotterEvent(Timed timed, int delaySeconds)
        {
            type = Type.TIMED;
            eventTimed = timed;
            this.delaySeconds = delaySeconds;
        }

        public string getFilePath()
        {
            return PotterConfig.Instance.getAudioPath() + @"\" + type.ToString().ToLower() + @"\" + getEventName() + @".mp3";
        }

        public SerialOut getSerialOut()
        {
            checkIsType(Type.EVENT);
            switch (serialIn)
            {
                case SerialIn.BANOCHKA_ZELIYA_P:
                case SerialIn.OTKRITIE_SUNDUKA_1:
                case SerialIn.OTKRITIE_ZASLONKI_LABIRINTA_2:
                    return SerialOut.VOSPOMINANIE_G_1;
                case SerialIn.USER_BERET_KNIGU_3:
                case SerialIn.PODNESENIE_NOSKA_4:
                case SerialIn.OTKRITIE_SKATULKI_DOBBI_9:
                    return SerialOut.VOSPOMINANIE_R_3;
                case SerialIn.NAGATIE_KIRPICHA_A:
                case SerialIn.KIRPICHI_RAZGADANY_B:
                    return SerialOut.VOSPOMINANIE_B_2;
            }
            return SerialOut.EMPTY;
        }

        public SerialOut getSerialOutManual()
        {
            checkIsType(Type.MANUAL);
            return serialOutManual;
        }

        private string getEventName()
        {
            switch (type)
            {
                case Type.TIMED:
                    return eventTimed.ToString().ToLower();
                case Type.MANUAL:
                    return eventManual.ToString().ToLower();
            }
            return serialIn.ToString().ToLower();
        }

        private void checkIsType(Type typeToCheck)
        {
            if (type != typeToCheck)
            {
                throw new InvalidOperationException("Ожидаемый тип объекта " + typeToCheck.ToString() + ", но текущий тип - " + type.ToString());
            }
        }
    }
}