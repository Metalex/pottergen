﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Potter
{
    internal class PotterConfig
    {
        // KEYS start 

        public const string KEY_PORT_NUMBER = "port_number";
        public const string KEY_AUDIO_PATH = "audio_path";
        public const string KEY_BOI_START_DELAY = "boi_start_delay";
        public const string KEY_FOREST_BACKGROUND_PATH = "forest_background_path";
        public const string KEY_BOI_BACKGROUND_PATH = "boi_background_path";
        public const string KEY_BOI_PODSKAZKA_DELAY = "boi_podskazka_delay";
        public const string KEY_BOI_POBEDA_DELAY = "boi_pobeda_delay";
        public const string KEY_POSLE_POBEDY_BACKGROUND_PATH = "posle_pobedy_background_path";

        // KEYS end

        private const string CONFIG_NAME = "potter_config.txt";

        private Dictionary<string, string> values = new Dictionary<string, string>();

        private static readonly Lazy<PotterConfig> lazy = new Lazy<PotterConfig>(() => new PotterConfig());
        public static PotterConfig Instance { get { return lazy.Value; } }
        private PotterConfig() { }

        public bool restore()
        {
            try
            {
                using (var fileStream = File.OpenRead(CONFIG_NAME))
                using (var streamReader = new StreamReader(fileStream, Encoding.UTF8, true, 16))
                {
                    string line;
                    while ((line = streamReader.ReadLine()) != null)
                    {
                        string[] splitted = line.Split(':');
                        if (splitted.Length >= 2)
                        {
                            values.Add(splitted[0], line.Substring(line.IndexOf(':')+1));
                        }
                        else
                        {
                            throw new InvalidDataException("Строка должна иметь формат ключ:значение");
                        }
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                DialogResult dialogResult = MessageBox.Show(e.Message, "Ошибка при чтении файла конфигурации", MessageBoxButtons.OK);
                if (dialogResult == DialogResult.OK)
                {
                    Application.Exit();
                }
                return false;
            }
        }

        public void save()
        {
            using (var fileStream = File.Open(CONFIG_NAME, FileMode.Create))
            using (var streamWriter = new StreamWriter(fileStream))
            {
                Dictionary<string, string>.Enumerator enumerator = values.GetEnumerator();
                foreach(KeyValuePair<string, string> entry in values)
                { 
                    streamWriter.WriteLine(entry.Key + ":" + entry.Value);
                }
            }
        }

        private int getValue(string key)
        {
            return values.ContainsKey(key) ? int.Parse(values[key]) : -1;
        }

        public int getPortNumber()
        {
            return getValue(KEY_PORT_NUMBER);
        }

        public void setPortNumber(int portNumber)
        {
            values[KEY_PORT_NUMBER] = portNumber + "";
        }

        public string getAudioPath()
        {
            return values[KEY_AUDIO_PATH];
        }

        public int getBoiStartDelay()
        {
            return getValue(KEY_BOI_START_DELAY);
        }

        public string getForestBackgroundPath()
        {
            return values[KEY_FOREST_BACKGROUND_PATH];
        }

        public string getBoiBackgroundPath()
        {
            return values[KEY_BOI_BACKGROUND_PATH];
        }

        public int getBoiPodskazkaDelay()
        {
            return getValue(KEY_BOI_PODSKAZKA_DELAY);
        }

        public int getBoiPobedaDelay()
        {
            return getValue(KEY_BOI_POBEDA_DELAY);
        }

        public string getPosleBobedyBackgroundPath()
        {
            return values[KEY_POSLE_POBEDY_BACKGROUND_PATH];
        }
    }
}