﻿using NAudio.Wave;
using Potter.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Potter
{

    public partial class Form1 : Form
    {

        PotterSerial serialPort;
        PotterOrder order = new PotterOrder();
        bool isFightRunning = false;
        bool isFightStarted = false;
        bool isFightFinished = false;
        long lastPlannedTime = -1;
        int eventTimedSecondsRemain = -1;
        int lastBackgroundVolume = -1;
        int lastEventsVolume = -1;
        bool doorIsOpened = false;
        long questStartTimeMillis = -1;
        long questGameTimeMillis = -1;

        // mic stuff
        bool isMicOn = false;
        BufferedWaveProvider micBuffer;
        WaveOut micOutput;
        WaveIn micInput;

        public Form1()
        {
            InitializeComponent();
            if (PotterConfig.Instance.getPortNumber() != -1)
            {
                portNumber.Text = PotterConfig.Instance.getPortNumber() + "";
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Console.SetOut(new RichTextBoxStreamWriter(consoleList));
        }

        private void start_Click(object sender, EventArgs e)
        {
            try {
                int portValue = int.Parse(portNumber.Text);
                serialPort = new PotterSerial(portValue);
                serialPort.addEventHandler(onSerialEvent);
                try
                {
                    serialPort.open();
                    configureBackground();
                    configureEvents();
                    configureBoiPrervan();
                    start.Enabled = false;
                    portNumber.Enabled = false;
                    questStart.Enabled = true;
                    PotterConfig.Instance.setPortNumber(portValue);
                    PotterConfig.Instance.save();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ошибка при открытии порта. Проверьте № порта", ex.Message);
                }
            } catch (Exception exc)
            {
                MessageBox.Show("Неправильный № порта", exc.Message);
            }
        }

        private void writeToSerial(int number)
        {
            Console.Write(PotterUtils.Instance.getTime() + " Запись в сериал порт: " + number + "\n");
            try
            {
                serialPort.write(number);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка при записи в порт", ex.Message);
            }
        }

        /*
        * Is invoked when some data comes from serial port. Worker Thread
        */
        private void onSerialEvent(string text)
        {
            BeginInvoke(new FromSerialToUi(showOnForm), new object[] { text });
        }

        /*
        * Is invoked by delagate in UI thread
        */
        private void showOnForm(string text)
        {
            Console.Write(PotterUtils.Instance.getTime() + " Считано с сериал порта: " + text + "\n");
            try {
                PotterEvent potterEvent = new PotterEvent(text);
                if (potterEvent.serialIn == PotterEvent.SerialIn.BOI_NACHAT_VOZOBNOVLEN_E)
                {
                    if (isFightFinished || isFightRunning)
                    {
                        return;
                    }
                    if (isFightStarted)
                    {
                        // бой возобновлен

                        // вернем громкости плеерам
                        if (lastBackgroundVolume != -1)
                        {
                            playerBackground.settings.volume = lastBackgroundVolume;
                            lastBackgroundVolume = -1;
                        }
                        if (lastEventsVolume != -1)
                        {
                            playerEvents.settings.volume = lastEventsVolume;
                            lastEventsVolume = -1;
                        }

                        // снова продолжим работу таймера, если он был запущен
                        if (eventTimedSecondsRemain > 0)
                        {
                            eventTimed.Interval = eventTimedSecondsRemain;
                            eventTimed.Start();
                            lastPlannedTime = PotterUtils.Instance.getMillis();
                            eventTimedSecondsRemain = -1;
                        }
                    } else
                    {
                        // бой начинается
                        // установим фон боя
                        setBackground(PotterConfig.Instance.getBoiBackgroundPath());
                        restoreBackground();
                        // поставим в очередь подсказку Дамблдора
                        order.clear();
                        PotterEvent eventBoiPodskazka = new PotterEvent(PotterEvent.Timed.BOI_PODSKAZKA,
                            PotterConfig.Instance.getBoiPodskazkaDelay());
                        playTimed(eventBoiPodskazka);
                        isFightStarted = true;
                    }
                    
                    // покажем, что бой запущен и может быть прерван
                    isFightRunning = true;
                } 
                else if (potterEvent.serialIn == PotterEvent.SerialIn.BOI_PRERVAN_G)
                {
                    if (isFightFinished || !isFightRunning)
                    {
                        return;
                    }

                    // бой приостановлен - мы должны сразу проиграть аудио "бой прерван"

                    // сохраним текущие громкости и заглушим фон и события
                    lastBackgroundVolume = playerBackground.settings.volume;
                    lastEventsVolume = playerEvents.settings.volume;
                    silentBackground();
                    playerEvents.settings.volume = 50;
                    // начнем проигрывание
                    boiPrervanPlayer.Ctlcontrols.stop();
                    boiPrervanPlayer.URL = potterEvent.getFilePath();
                    try
                    {
                        boiPrervanPlayer.Ctlcontrols.play();
                    }
                    catch (ArgumentException e)
                    {
                        MessageBox.Show("Не могу проиграть файл события", e.Message);
                    }

                    // если есть отложенный таймер - мы должны поставить его на паузу
                    if (lastPlannedTime != -1)
                    {
                        // рассчитаем интервал для будущего таймера
                        int secondsElapsed = (int) (PotterUtils.Instance.getMillis() - lastPlannedTime);
                        eventTimedSecondsRemain = eventTimed.Interval - secondsElapsed;
                        eventTimed.Stop();
                        lastPlannedTime = -1;
                    }

                    isFightRunning = false;
                } 
                else {
                    if (potterEvent.serialIn == PotterEvent.SerialIn.PROHOGDENIE_SHAHMAT_D)
                    {
                        doorIsOpened = true;
                    }
                    playAndOrder(potterEvent);
                }
                
            } catch (Exception e)
            {
                Console.Write(PotterUtils.Instance.getTime() + " " + e.Message + "\n");
                // Bad input from serial port
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PotterEvent potterEvent = new PotterEvent(PotterEvent.Manual.QUEST_START, PotterEvent.SerialOut.EMPTY);
            playAndOrder(potterEvent);
            questStart.Enabled = false;
            boiOkonchenDobbi.Enabled = true;
            mic.Enabled = true;
            labelQuestStartTime.Visible = true;
            labelQuestGameTime.Visible = true;
            buttonStopTime.Visible = true;
            // посылаем событие на сериал порт, что пора включить казан
            writeToSerial((int)PotterEvent.SerialOut.KAZAN_START_5);
            // засекаем время начала квеста
            questStartTimeMillis = PotterUtils.Instance.getMillis();
            // выводим время начала квеста
            labelQuestStartTime.Text = "НАЧАЛО: " + PotterUtils.Instance.generalMillisToTime(questStartTimeMillis);
            // стартуем вывод прошедшего времени
            timerTime.Interval = 1000; // обновляем раз в секунду
            timerTime.Start();
        }

        private void boiOkonchenDobbi_Click(object sender, EventArgs e)
        {
            PotterEvent potterEvent = new PotterEvent(PotterEvent.Manual.BOI_OKONCHEN_DOBBI, PotterEvent.SerialOut.EMPTY);
            playAndOrder(potterEvent);
            boiOkonchenDobbi.Enabled = false;
            playHappyBirthday.Enabled = true;
        }

        private void playHappyBirthday_Click(object sender, EventArgs e)
        {
            PotterEvent potterEvent = new PotterEvent(PotterEvent.Manual.HAPPY_BIRTHDAY, PotterEvent.SerialOut.PODAROK_4);
            playAndOrder(potterEvent);
            playHappyBirthday.Enabled = false;
        }

        private void Form1_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            stopMic();
            serialPort.close();
            playerBackground.Ctlcontrols.stop();
            playerEvents.Ctlcontrols.stop();
        }

        private void configureBackground()
        {
            var myPlayList = playerBackground.playlistCollection.newPlaylist("Background");

            try {
                string path = PotterConfig.Instance.getAudioPath();
                foreach (string media in Directory.GetFiles(path))
                {
                    var mediaItem = playerBackground.newMedia(media);
                    myPlayList.appendItem(mediaItem);
                }
            } catch (DirectoryNotFoundException e)
            {
                MessageBox.Show(@"Нет папки D:\Audio", "Ошибка");
            }

            playerBackground.currentPlaylist = myPlayList;
            playerBackground.settings.volume = 100;
            try {
                playerBackground.Ctlcontrols.play();
            }
            catch (ArgumentException e)
            {
                MessageBox.Show("Не могу проиграть фоновый плейлист", e.Message);
            }
        }

        private void configureEvents()
        {
            playerEvents.settings.volume = 100;
            playerEvents.PlayStateChange += PlayerEvents_PlayStateChange;
        }

        private void configureBoiPrervan()
        {
            boiPrervanPlayer.settings.volume = 100;
        }

        private void silentBackground()
        {
            setVolumeBackground(20);
        }

        private void restoreBackground()
        {
            setVolumeBackground(100);
        }

        private void setVolumeBackground(int volume)
        {
            playerBackground.settings.volume = volume;
        }

        private void playAndOrder(PotterEvent potterEvent)
        {
            order.add(potterEvent);
            play(potterEvent);
        }

        private void play(PotterEvent potterEvent)
        {
            if (potterEvent.type == PotterEvent.Type.TIMED &&
                potterEvent.eventTimed == PotterEvent.Timed.BOI_PODSKAZKA)
            {
                // начала проигрываться подсказка, поэтому сбросим переменную в -1, чтобы 
                // при следующем разрыве и возобновлении мы не начали проигрывать подсказку 
                lastPlannedTime = -1;
            }
            if (potterEvent.type == PotterEvent.Type.TIMED &&
                potterEvent.eventTimed == PotterEvent.Timed.BOI_POBEDA)
            {
                playerBackground.Ctlcontrols.stop();
                playerEvents.settings.volume = 100;
                isFightFinished = true;
                stopTime();
            }
            if (playerEvents.playState == WMPLib.WMPPlayState.wmppsPlaying ||
                playerEvents.playState == WMPLib.WMPPlayState.wmppsPaused ||
                isMicOn && !doorIsOpened)
            {
                // if now is playing or mic is on - do not anything
            }
            else {
                playerEvents.Ctlcontrols.stop();
                playerEvents.URL = potterEvent.getFilePath();
                try
                {
                    playerEvents.Ctlcontrols.play();
                    if (potterEvent.type == PotterEvent.Type.EVENT &&
                        potterEvent.getSerialOut() != PotterEvent.SerialOut.EMPTY)
                    {
                        writeToSerial((int)potterEvent.getSerialOut());
                    } else if (potterEvent.type == PotterEvent.Type.MANUAL &&
                        potterEvent.getSerialOutManual() != PotterEvent.SerialOut.EMPTY)
                    {
                        writeToSerial((int)potterEvent.getSerialOutManual());
                    }
                    
                }
                catch (ArgumentException e)
                {
                    MessageBox.Show("Не могу проиграть файл события", e.Message);
                }
            }
            updateOrderLabel();
        }

        private void playTimed(PotterEvent potterEvent)
        {
            if (!isFightStarted || isFightRunning)
            {
                eventTimed.Interval = potterEvent.delaySeconds * 1000;
                eventTimed.Start();
                lastPlannedTime = PotterUtils.Instance.getMillis();
            } else
            {
                // это может произойти только для события победы
                // если пользователь во время проигрывания аудио о силе Когтевран (за которым следует победа)
                // прервал бой, то playTimed вызовется когда isFightRunning будет false
                eventTimedSecondsRemain = potterEvent.delaySeconds * 1000;
                // в таком случае, мы проиграем это событие только когда бой будет возобновлен
            }
            order.add(potterEvent);
            updateOrderLabel();
        }

        private void eventTimed_Tick(object sender, EventArgs e)
        {
            play(order.peek());
            eventTimed.Stop();
            lastPlannedTime = -1;
        }

        private void updateOrderLabel()
        {
            orderList.ResetText();
            bool first = true;
            foreach (PotterEvent orderItem in order)
            {
                orderList.SelectionFont = new Font(orderList.Font, first ? FontStyle.Bold : FontStyle.Regular);
                orderList.AppendText(orderItem.getFilePath() + "\n");
                first = false;
            }
        }

        private void PlayerEvents_PlayStateChange(object sender, AxWMPLib._WMPOCXEvents_PlayStateChangeEvent e)
        {
            if (e.newState == (int) WMPLib.WMPPlayState.wmppsPlaying)
            {
                silentBackground();
                playerEvents.Visible = true;
            }
            else if (e.newState == (int) WMPLib.WMPPlayState.wmppsMediaEnded)
            {
                PotterEvent endedEvent = order.pop();
                // аудио, что шахматы разгаданы, закончено
                if (endedEvent.type == PotterEvent.Type.EVENT &&
                    endedEvent.serialIn == PotterEvent.SerialIn.PROHOGDENIE_SHAHMAT_D)
                {
                    order.clear();
                    // поставим в очередь приглашение Волана к бою
                    PotterEvent eventBoiStart = new PotterEvent(PotterEvent.Timed.BOI_START,
                        PotterConfig.Instance.getBoiStartDelay());
                    playTimed(eventBoiStart);
                    // включим как фон звук леса
                    setBackground(PotterConfig.Instance.getForestBackgroundPath());
                    setVolumeBackground(70);
                } else if (endedEvent.type == PotterEvent.Type.EVENT &&
                    endedEvent.serialIn == PotterEvent.SerialIn.BOI_SILA_KOGTEVRAN_M)
                {
                    order.clear();
                    // поставим в очередь сообщение о победе
                    PotterEvent eventBoiPobeda = new PotterEvent(PotterEvent.Timed.BOI_POBEDA,
                        PotterConfig.Instance.getBoiPobedaDelay());
                    playTimed(eventBoiPobeda);
                } else if (endedEvent.type == PotterEvent.Type.TIMED &&
                    endedEvent.eventTimed == PotterEvent.Timed.BOI_POBEDA)
                {
                    order.clear();
                    setBackground(PotterConfig.Instance.getPosleBobedyBackgroundPath());
                    restoreBackground();
                }
                else {
                    if (order.isEmpty())
                    {
                        restoreBackground();
                        playerEvents.Visible = false;
                        writeToSerial((int)PotterEvent.SerialOut.VOSPOMINANIE_OFF_0);
                        updateOrderLabel();
                    }
                    else
                    {
                        playTimer.Enabled = true;
                    }
                }
            }
        }

        private void playTimer_Tick(object sender, EventArgs e)
        {
            playTimer.Enabled = false;
            play(order.peek());
        }

        private void mic_Click(object sender, EventArgs e)
        {
            isMicOn = !isMicOn;
            mic.Image = isMicOn ? Resources.ic_mic : Resources.ic_mic_off;
            if (isMicOn)
            {
                startMic();
            } else
            {
                stopMic();
            }
        }

        private void startMic()
        {
            micOutput = new WaveOut();
            micInput = new WaveIn();

            micInput.DataAvailable += new EventHandler<WaveInEventArgs>(wi_DataAvailable);

            micBuffer = new BufferedWaveProvider(micInput.WaveFormat);
            micBuffer.DiscardOnBufferOverflow = true;

            micOutput.Init(micBuffer);
            micInput.StartRecording();
            micOutput.Play();

            if (!doorIsOpened)
            {
                silentBackground();
                if (playerEvents.playState == WMPLib.WMPPlayState.wmppsPlaying)
                {
                    playerEvents.Ctlcontrols.pause();
                }
            }
        }

        private void stopMic()
        {
            if (micOutput != null)
            {
                micOutput.Stop();
                micOutput = null;
            }
            if (micInput != null)
            {
                micInput.Dispose();
                micInput = null;
            }
            micBuffer = null;

            if (!doorIsOpened)
            {
                if (playerEvents.playState == WMPLib.WMPPlayState.wmppsPaused)
                {
                    playerEvents.Ctlcontrols.play();
                }
                else if (playerEvents.playState == WMPLib.WMPPlayState.wmppsStopped && !order.isEmpty())
                {
                    play(order.peek());
                }
                else
                {
                    restoreBackground();
                }
            }
        }

        private void wi_DataAvailable(object sender, WaveInEventArgs e)
        {
            micBuffer.AddSamples(e.Buffer, 0, e.BytesRecorded);
        }
        
        private void setBackground(string path)
        {
            playerBackground.Ctlcontrols.stop();
            playerBackground.URL = PotterConfig.Instance.getAudioPath() + @"\" + path;
            playerBackground.settings.setMode("loop", true);
            try
            {
                playerBackground.Ctlcontrols.play();
            }
            catch (ArgumentException e)
            {
                MessageBox.Show("Не могу проиграть файл фона", e.Message);
            }
        }

        private void checkDebug_CheckedChanged(object sender, EventArgs e)
        {
            consoleList.Visible = !consoleList.Visible;
            orderListClear.Visible = !orderListClear.Visible;
            buttonPodarokSerial.Visible = !buttonPodarokSerial.Visible;
            buttonDobbyReset.Visible = !buttonDobbyReset.Visible;
            buttonOpenGreenCasket.Visible = !buttonOpenGreenCasket.Visible;
            this.Size = new System.Drawing.Size(625, consoleList.Visible ? 500 : 400);
        }

        private void timerTime_Tick(object sender, EventArgs e)
        {
            questGameTimeMillis = PotterUtils.Instance.getMillis() - questStartTimeMillis;
            labelQuestGameTime.Text = "ПРОШЛО: " + PotterUtils.Instance.elapsedMillisToTime(questGameTimeMillis);
        }

        private void buttonStopTime_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Нажмите Да, чтобы остановить время прохождения квеста", "Внимание!", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                stopTime();
            }
        }

        private void stopTime()
        {
            timerTime.Stop();
            buttonStopTime.Visible = false;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Закрыть программу?", "Внимание!", MessageBoxButtons.YesNo);
            if (dialogResult != DialogResult.Yes)
            {
                e.Cancel = true;
            }
        }

        private void orderListClear_Click(object sender, EventArgs e)
        {
            order.removeLast();
            updateOrderLabel();
        }

        private void buttonPodarokSerial_Click(object sender, EventArgs e)
        {
            writeToSerial((int)PotterEvent.SerialOut.PODAROK_4);
        }

        private void buttonDobbyReset_Click(object sender, EventArgs e)
        {
            writeToSerial((int)PotterEvent.SerialOut.DOBBY_RESET_6);
        }

        private void buttonOpenGreenCasket_Click(object sender, EventArgs e)
        {
            writeToSerial((int)PotterEvent.SerialOut.OPEN_GREEN_CASKET_7);
        }
    }
}
